const path = require("path");

module.exports = {
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    development: {
      host: "localhost",
      port: 7545,
      network_id: "5777",
    },
    test: {
      host: "localhost",
      port: 7545,
      network_id: "1234",
    },
    // live: { ... }
  },
  compilers: {
    solc: {
      settings: {
        optimizer: { enabled: true, runs: 200 },
      },
    },
  },
};
