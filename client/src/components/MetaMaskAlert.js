import React, { Component } from 'react';
import metamaskLogo from '../images/Metamask.png'

class MetaMaskAlert extends Component {
    render() {
        return (
            <div className="my-5 text-center">
                <img src={metamaskLogo} width="250" class="mb-4" alt="" />
                <h1>¡Instala MetaMask para acceder a todas las funcionalidades!</h1>
            </div>
        );
    }
}
export default MetaMaskAlert;