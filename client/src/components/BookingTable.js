import React from "react";
import Table from "react-bootstrap/Table";
import { weiToEth, timestampToDate, idState } from "../utils/auxiliars";
import * as SimpleIcon from "react-icons/si";
import Button from "react-bootstrap/Button";

import "./BookingTable.css";

export default function BookingTable({ bookingListFiltered, sendAction }) {
  const handleClick = (id, action) => {
    sendAction(id, action);
  };

  // onClick should be a function and not a function call **fixed

  return (
    <Table striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>#</th>
          <th>Fecha Inicio</th>
          <th>Fecha Fin</th>
          <th>
            Fondos disponibles
            <SimpleIcon.SiEthereum />
          </th>
          <th>Estado</th>
          <th>User</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        {bookingListFiltered.map((booking, key) => {
          return (
            <tr key={key}>
              <th>{booking.id}</th>
              <th>{timestampToDate(booking.startDate)}</th>
              <th>{timestampToDate(booking.endDate)}</th>
              <th>{weiToEth(booking.ownerPendingFunds)}</th>
              <th>{idState[booking.state]}</th>
              <th>{booking.user}</th>
              <th>
                <div className="grid-container">
                  <div className="grid-item">
                    <Button
                      onClick={function () {
                        booking.state === "0"
                          ? handleClick(booking.id, "accept")
                          : alert("El estado de la reserva no es valido");
                      }}
                      variant="success"
                    >
                      Aceptar
                    </Button>
                  </div>
                  <div className="grid-item">
                    <Button
                      onClick={function () {
                        booking.state === "0"
                          ? handleClick(booking.id, "reject")
                          : alert("No puede cancelar la reserva actualemente");
                      }}
                      variant="danger"
                    >
                      Rechazar
                    </Button>
                  </div>
                  <div className="grid-item">
                    <Button
                      onClick={function () {
                        (booking.state === "2" || booking.state === "4") &&
                        weiToEth(booking.ownerPendingFunds)
                          ? handleClick(booking.id, "withdraw-owner")
                          : alert(
                              "No puedes reclamar los fondos de esta reserva"
                            );
                      }}
                      variant="light"
                    >
                      Withdraw
                    </Button>
                  </div>
                  <div className="grid-item">
                    <Button
                      onClick={function () {
                        booking.state === "2" &&
                        parseInt(booking.endDate) > Date.now()
                          ? handleClick(booking.id, "check-booking")
                          : alert(
                              "No puedes comprobar el problema con la reserva aun"
                            );
                      }}
                      variant="warning"
                    >
                      Check Issue
                    </Button>
                  </div>
                </div>
              </th>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}
