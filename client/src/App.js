import React, { useState, useEffect } from "react";
import Aplicacion from "./contracts/Aplicacion.json";

import "./App.css";

// Useful variables and functions
import { daysInBetween, deposit } from "./utils/auxiliars";
// Components Import
import MetaMaskAlert from "./components/MetaMaskAlert";
import NavbarCustom from "./components/NavbarCustom";
import Loader from "./components/Loader";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Pages
import Home from "./pages/Home";
import Apartments from "./pages/Apartments";
import NewApartment from "./pages/NewApartment";
import ViewApartment from "./pages/ViewApartment";
import AboutUs from "./pages/AboutUs";
import ApartmentsOwned from "./pages/ApartmentsOwned";
import ViewApartmentOwned from "./pages/ViewApartmentOwned";
import PageNotFound from "./pages/PageNotFound";
import UserBookings from "./pages/UserBookings";
import BookingView from "./pages/BookingView";
import ApartmentEdition from "./pages/AparmentEdition";

import getWeb3 from "./getWeb3";

function App() {
  const [nApartments, setNApartments] = useState(0);
  const [apartments, setApartments] = useState([]);
  const [nBookings, setNBookings] = useState(0);
  const [bookings, setBookings] = useState([]);
  const [web3, setWeb3] = useState(undefined);
  const [currentAccount, setCurrentAccount] = useState(undefined);
  const [accountsChanged, setAccountsChanged] = useState(false);
  const [loadEvents, setLoadEvents] = useState(false);
  const [metamaskInstalled, setMetaMaskInstalled] = useState(false);
  const [app, setApp] = useState(undefined);
  const [loading, setLoading] = useState(false);
  const [loadData, setLoadData] = useState(false);
  const [updateApartments, setUpdateApartments] = useState(false);
  const [updateBookings, setUpdateBookings] = useState(false);

  // EFFECTS USED IN THE APP
  useEffect(() => {
    // Detect Metamask
    const metamaskInstalled = typeof window.ethereum !== "undefined";

    setMetaMaskInstalled(metamaskInstalled);
    if (metamaskInstalled) {
      console.log("MetaMask is installed!");
      setLoading(true);
      setLoadEvents(true);
    }
  }, []);

  useEffect(() => {
    if (metamaskInstalled && loading) {
      console.log("Effect load web3");
      const init = async () => {
        // Get network provider and web3 instance
        // To access window.ethereum properties user web3.givenProvider
        const web3 = await getWeb3();

        // Promise resolve
        const currentAccount = await web3.givenProvider.request({
          method: "eth_accounts",
        });

        // Si ya nos habiamos conectado previamente, cargar la cuenta
        currentAccount[0] !== undefined
          ? setCurrentAccount(currentAccount[0])
          : console.log("Account not set yet");

        // Get network ID from browser
        const networkId = await web3.eth.net.getId();

        // Get json for deployed network
        const deployedNetwork = Aplicacion.networks[networkId];
        console.log("Direccion del contrato: " + deployedNetwork.address);

        // Cargar los datos de la blockchain
        if (deployedNetwork) {
          // Load app
          const app = new web3.eth.Contract(
            Aplicacion.abi,
            deployedNetwork.address
          );
          setApp(app);
          setLoadData(true);
        } else {
          window.alert("app contract not deployed to detected network.");
        }
        setWeb3(web3);
      };
      init();
    }
  }, [metamaskInstalled, loading]);

  useEffect(() => {
    if (loading && app && loadData) {
      console.log("Effect Load Blockchain Data");
      const readFromBlockchain = async () => {
        let nApartments, nBookings, apartments, bookings;
        nApartments = await app.methods.aCont().call();
        if (nApartments > 0) {
          apartments = [];
          nBookings = await app.methods.bCont().call();
          if (nBookings > 0) {
            for (let i = 0; i < nApartments; i++) {
              const apartment = await app.methods.apartments(i).call();
              // Add bookings to the apartment data
              apartment["bookingIDs"] = await app.methods
                .getBookings(apartment.id)
                .call();
              apartments.push(apartment);
            }

            bookings = [];
            for (let i = 0; i < nBookings; i++) {
              const booking = await app.methods.bookings(i).call();
              if (currentAccount) {
                booking["ownerPendingFunds"] = await app.methods
                  .getOwnerPendingWithdraws(currentAccount, booking.id)
                  .call();
                booking["userPendingFunds"] = await app.methods
                  .getUserPendingWithdraws(currentAccount, booking.id)
                  .call();
              }
              bookings.push(booking);
            }
          } else {
            for (let i = 0; i < nApartments; i++) {
              const apartment = await app.methods.apartments(i).call();
              // Add bookings to the apartment data
              apartment["bookingIDs"] = [];
              apartments.push(apartment);
            }
          }
        } else {
          nApartments = 0;
          nBookings = 0;
          apartments = [];
          bookings = [];
        }

        setNApartments(nApartments);
        setApartments(apartments);

        setNBookings(nBookings);
        setBookings(bookings);

        setLoading(false);
        setLoadData(false);
      };
      readFromBlockchain();
    }
  }, [app, loading, loadData, currentAccount]);

  useEffect(() => {
    if (updateApartments && loading && app) {
      console.log("Effect refresh apartments data");
      const refreshData = async () => {
        // Si existian datos ya añadir los nuevos
        if (apartments && nApartments) {
          console.log("\tAdding only new apartments");
          const actualNumberApartments = await app.methods.aCont().call();
          let newApartments = [];
          for (let i = nApartments; i < actualNumberApartments; i++) {
            const apartment = await app.methods.apartments(i).call();
            apartment["bookingIDs"] = await app.methods
              .getBookings(apartment.id)
              .call();
            newApartments.push(apartment);
          }
          setNApartments(actualNumberApartments);
          setApartments(apartments.concat(newApartments));
        } else {
          // Se ejecutaría a la hora de añadir la primera vivienda
          console.log("\tAdding first apartment");
          let nApartments = await app.methods.aCont().call();
          let apartments = [];
          for (let i = 0; i < nApartments; i++) {
            const apartment = await app.methods.apartments(i).call();
            // Add bookings to the apartment data
            apartment["bookingIDs"] = await app.methods
              .getBookings(apartment.id)
              .call();
            apartments.push(apartment);
          }
          setNApartments(nApartments);
          setApartments(apartments);
        }

        setLoading(false);
      };
      refreshData();
      setUpdateApartments(false);
    }
  }, [updateApartments, loading, apartments, nApartments, app]);

  useEffect(() => {
    if (updateBookings && loading && app && apartments) {
      console.log("Effect refresh booking data");
      const refreshData = async () => {
        // Si ya existen reservas, se actualiza con las nuevas
        if (bookings && nBookings) {
          console.log("\tAdding only new bookings");
          const actualNumberBookings = await app.methods.bCont().call();
          let newBookings = [];
          for (let i = nBookings; i < actualNumberBookings; i++) {
            const booking = await app.methods.bookings(i).call();
            if (currentAccount) {
              booking["ownerPendingFunds"] = await app.methods
                .getOwnerPendingWithdraws(currentAccount, booking.id)
                .call();
              booking["userPendingFunds"] = await app.methods
                .getUserPendingWithdraws(currentAccount, booking.id)
                .call();
            }
            // Update apartment booking list
            setApartments(
              apartments.map((apartment) =>
                apartment.id === booking.apartmentId
                  ? {
                      ...apartment,
                      bookingIDs: [...apartment.bookingIDs, booking.id],
                    }
                  : apartment
              )
            );
            newBookings.push(booking);
          }

          setNBookings(actualNumberBookings);
          setBookings(bookings.concat(newBookings));
        } else {
          // Solo se ejecuta con la primera reserva
          console.log("\tAdding fist booking");
          let nBookings, bookings;
          nBookings = await app.methods.bCont().call(); // 1
          bookings = [];
          for (let i = 0; i < nBookings; i++) {
            const booking = await app.methods.bookings(i).call();
            if (currentAccount) {
              booking["ownerPendingFunds"] = await app.methods
                .getOwnerPendingWithdraws(currentAccount, booking.id)
                .call();
              booking["userPendingFunds"] = await app.methods
                .getUserPendingWithdraws(currentAccount, booking.id)
                .call();
            }
            // Update apartment booking list
            setApartments(
              apartments.map((apartment) =>
                apartment.id === booking.apartmentId
                  ? {
                      ...apartment,
                      bookingIDs: [...apartment.bookingIDs, booking.id],
                    }
                  : apartment
              )
            );
            bookings.push(booking);
          }
          setNBookings(nBookings);
          setBookings(bookings);
        }

        setLoading(false);
      };
      refreshData();
      setUpdateBookings(false);
    }
  }, [
    loading,
    app,
    apartments,
    bookings,
    nBookings,
    updateBookings,
    currentAccount,
  ]);

  useEffect(() => {
    if (loading && app && apartments && accountsChanged) {
      setAccountsChanged(false);
      console.log("Effect refresh user booking data");
      const refreshData = async () => {
        // Actualizar todas las reservas asociadas al apartamento
        if (bookings && nBookings) {
          let nBookings, bookings;
          nBookings = await app.methods.bCont().call(); // 1
          bookings = [];
          for (let i = 0; i < nBookings; i++) {
            const booking = await app.methods.bookings(i).call();
            if (currentAccount) {
              booking["ownerPendingFunds"] = await app.methods
                .getOwnerPendingWithdraws(currentAccount, booking.id)
                .call();
              booking["userPendingFunds"] = await app.methods
                .getUserPendingWithdraws(currentAccount, booking.id)
                .call();
            }
            // Update apartment booking list
            setApartments(
              apartments.map((apartment) =>
                apartment.id === booking.apartmentId
                  ? {
                      ...apartment,
                      bookingIDs: [...apartment.bookingIDs, booking.id],
                    }
                  : apartment
              )
            );
            bookings.push(booking);
          }
          setBookings(bookings);
        }
        setLoading(false);
      };
      refreshData();
    }
  }, [
    loading,
    app,
    apartments,
    bookings,
    nBookings,
    currentAccount,
    accountsChanged,
  ]);

  // EVENTS && HANDLERS
  function handleAccountChange(account) {
    console.log("Account did change");
    setCurrentAccount(account);
    setAccountsChanged(true);
    setLoading(true);
  }

  useEffect(() => {
    if (metamaskInstalled && loadEvents) {
      console.log("Loading eth events on accounts");
      window.ethereum.on("accountsChanged", (accounts) => {
        handleAccountChange(accounts[0]);
      });
      setLoadEvents(false);
    }
  }, [metamaskInstalled, loadEvents]);

  // SMART CONTRACT INTERACTION AND AUXILIAR FUNCTIONS

  async function connectWallet() {
    const currentAccount = await web3.givenProvider.request({
      method: "eth_requestAccounts",
    });
    setCurrentAccount(currentAccount);
  }

  function createApartment(title, description, direction, price, codigo) {
    setLoading(true);
    app.methods
      .addApartment(title, description, direction, price, codigo)
      .send({ from: currentAccount })
      .once("receipt", (receipt) => {
        setUpdateApartments(true);
      })
      .on("error", (error) => {
        setLoading(false);
        console.log("Apartment creation failed");
      });
  }

  function getBookingPricing(apartmentID, startDateUnix, endDateUnix) {
    let days = daysInBetween(startDateUnix, endDateUnix);
    console.log("Dias: " + days);
    return apartments[apartmentID].price * days + deposit;
  }

  function createBooking(apartmentID, startDateUnix, endDateUnix) {
    setLoading(true);
    let startDateParsed = parseInt(startDateUnix);
    let endDateParsed = parseInt(endDateUnix);
    let apartmentIdParsed = parseInt(apartmentID);

    // Fix deposit value
    let bookingPrice = getBookingPricing(
      apartmentIdParsed,
      startDateParsed,
      endDateParsed
    );

    console.log(
      "Sending booking data...\nApartment ID: " +
        apartmentIdParsed +
        " type: " +
        typeof apartmentIdParsed +
        "\nStart date: " +
        startDateParsed +
        " type: " +
        typeof startDateParsed +
        "\nEnd date: " +
        endDateParsed +
        " type: " +
        typeof endDateParsed +
        "\nCantidad: " +
        bookingPrice +
        " type: " +
        typeof bookingPrice
    );
    app.methods
      .book(apartmentIdParsed, startDateParsed, endDateParsed)
      .send({ from: currentAccount, value: bookingPrice })
      .once("receipt", (receipt) => {
        setUpdateBookings(true);
      })
      .on("error", (error) => {
        setLoading(false);
        console.log("booking failed");
      });
  }

  function apartmentsFilter(apartmentList) {
    try {
      return apartmentList.filter(
        (apartment) =>
          apartment.owner.toLowerCase() === currentAccount.toLowerCase()
      );
    } catch (error) {
      return [];
    }
  }

  async function updateBooking(bookingID) {
    console.log("Actualizando la reserva " + bookingID + "...");
    const bookingUpdated = await app.methods.bookings(bookingID).call();
    bookingUpdated["ownerPendingFunds"] = await app.methods
      .getOwnerPendingWithdraws(currentAccount, bookingID)
      .call();
    bookingUpdated["userPendingFunds"] = await app.methods
      .getUserPendingWithdraws(currentAccount, bookingID)
      .call();
    setBookings(
      bookings.map((booking) =>
        booking.id === bookingID
          ? {
              ...bookingUpdated,
            }
          : booking
      )
    );
  }

  async function updateAllBookings(bookingId) {
    const bookingIDs = await app.methods
      .getBookings(bookings[bookingId].apartmentId)
      .call();
    console.log("Actualizando todas las reservas asociadas...");

    for (let i = 0; i < bookingIDs.length; i++) {
      let bookingUpdated = await app.methods.bookings(bookingIDs[i]).call();
      bookingUpdated["ownerPendingFunds"] = await app.methods
        .getOwnerPendingWithdraws(currentAccount, bookingUpdated.id)
        .call();
      bookingUpdated["userPendingFunds"] = await app.methods
        .getUserPendingWithdraws(currentAccount, bookingUpdated.id)
        .call();
      setBookings(
        bookings.map((booking) =>
          booking.id === bookingUpdated.id
            ? {
                ...bookingUpdated,
              }
            : booking
        )
      );
    }
  }

  async function getCodeFromBooking(bookingID) {
    let code = await app.methods.viewCode(bookingID, currentAccount).call();
    alert(`El codigo de acceso para la reserva ${bookingID} es ${code}`);
  }

  function bookingsFilteredByUser(bookingList) {
    try {
      return bookingList.filter(
        (booking) => booking.user.toLowerCase() === currentAccount.toLowerCase()
      );
    } catch (error) {
      return [];
    }
  }

  async function updateApartment(id) {
    console.log("Actualizando la vivienda " + id + "...");
    const apartmentUpdated = await app.methods.apartments(id).call();
    apartmentUpdated["bookingIDs"] = await app.methods.getBookings(id).call();
    setApartments(
      apartments.map((apartment) =>
        apartment.id === id
          ? {
              ...apartmentUpdated,
            }
          : apartment
      )
    );
  }

  function editApartment(values) {
    const [id, title, description, direction, price, accessCode] = values;
    setLoading(true);
    app.methods
      .changeApartmentInfo(id, title, description, direction, price, accessCode)
      .send({ from: currentAccount })
      .once("receipt", (receipt) => {
        updateApartment(id);
        setLoading(false);
      })
      .on("error", (error) => {
        setLoading(false);
        console.log("Apartment creation failed");
      });
  }

  function changeApartmentState(id, action) {
    setLoading(true);
    switch (action) {
      case "disable-booking":
        app.methods
          .pauseBooking(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateApartment(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Apartment availability modification failed");
          });
        break;
      case "enable-booking":
        app.methods
          .resumeBooking(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateApartment(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Apartment availability modification failed");
          });
        break;
        break;
      default:
        break;
    }
  }

  function changeBookingState(id, action) {
    setLoading(true);
    switch (action) {
      case "accept":
        app.methods
          .confirmBooking(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateAllBookings(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking confirmation failed");
          });
        break;
      case "reject":
        app.methods
          .rejectBooking(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateBooking(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking rejection failed");
          });
        break;
      case "checkin":
        app.methods
          .checkin(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateBooking(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking check in failed");
          });
        break;
      case "checkout":
        app.methods
          .checkout(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateBooking(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking check out failed");
          });
        break;
      case "cancel-on-time":
      case "cancel-off-time":
        app.methods
          .cancelBooking(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateBooking(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking cancel failed");
          });
        break;
      case "withdraw-owner":
        app.methods
          .withdrawBooking(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateBooking(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking withdraw owner failed");
          });
        break;
      case "withdraw-user":
        app.methods
          .withdrawFunds(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateBooking(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking withdraw user failed");
          });
        break;
      case "check-booking":
        app.methods
          .checkBookingNotUsed(id)
          .send({ from: currentAccount })
          .once("receipt", (receipt) => {
            updateBooking(id);
            setLoading(false);
          })
          .on("error", (error) => {
            setLoading(false);
            console.log("Booking check failed");
          });
        break;
      case "view-code":
        try {
          getCodeFromBooking(id);
          setLoading(false);
        } catch (err) {
          setLoading(false);
          console.log("View code failed");
        }
        break;
      default:
        break;
    }
  }

  let web3content;
  if (loading) {
    web3content = <Loader />;
  } else {
    web3content = (
      <Switch>
        <Route path="/" exact /* important to avoid overlinking*/>
          <Home apartmentsList={apartments} />
        </Route>
        <Route path="/apartments/:apartmentId">
          <ViewApartment
            bookingList={bookings}
            userAccount={currentAccount}
            apartmentsList={apartments}
            onCreateBooking={(id, start, end) => createBooking(id, start, end)}
          />
        </Route>
        <Route path="/apartments">
          <Apartments
            apartmentsList={apartments}
            userAccount={currentAccount}
          />
        </Route>
        <Route path="/new-apartment">
          <NewApartment onCreateApartment={createApartment} />
        </Route>
        <Route path="/my-apartments/edition/:apartmentId">
          <ApartmentEdition
            apartmentsList={apartments}
            onEditApartment={(...values) => editApartment(values)}
          />
        </Route>
        <Route path="/my-apartments/:apartmentId">
          <ViewApartmentOwned
            userAccount={currentAccount}
            apartmentsList={apartments}
            bookingList={bookings}
            handleAction={(id, action) => changeBookingState(id, action)}
            handleApartmentState={(id, action) =>
              changeApartmentState(id, action)
            }
          />
        </Route>
        <Route path="/my-apartments">
          <ApartmentsOwned apartmetsFiltered={apartmentsFilter(apartments)} />
        </Route>
        <Route path="/bookings/:apartmentId">
          <BookingView
            userAccount={currentAccount}
            apartmentsList={apartments}
            bookingList={bookings}
            handleAction={(id, action) => changeBookingState(id, action)}
          />
        </Route>
        <Route path="/bookings">
          <UserBookings
            userAccount={currentAccount}
            apartmentList={apartments}
            bookingsFiltered={bookingsFilteredByUser(bookings)}
          />
        </Route>
        <Route>
          <PageNotFound />
        </Route>
      </Switch>
    );
  }

  let regularContent = (
    <Switch>
      <Route path="/" exact>
        <MetaMaskAlert />
      </Route>
      <Route path="/about-us">
        <AboutUs />
      </Route>
    </Switch>
  );

  return (
    <Router>
      <NavbarCustom
        isConnected={currentAccount}
        onConnectClick={connectWallet}
      />
      {metamaskInstalled ? web3content : regularContent}
    </Router>
  );
}

export default App;
