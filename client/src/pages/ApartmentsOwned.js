import React from "react";
import casa from "../images/casa.png";
import "./Apartments.css";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

export default function ApartmentsOwned({ apartmetsFiltered }) {
  let content;
  if (apartmetsFiltered.length > 0) {
    content = (
      <div className="main-container">
        <div className="upper-container">
          <div className="apartments-content">
            <h1 className="header">Estas son tus viviendas</h1>
            <p className="header-subtitle">
              ¡Puedes consultar su estado y gestionarlas!
            </p>
            <div className="container">
              <div class="row">
                {apartmetsFiltered.map((apartment, key) => {
                  return (
                    <div key={key}>
                      <div class="col">
                        <Card style={{ width: "21rem", marginBottom: "2rem" }}>
                          <Card.Img variant="top" src={casa} />
                          <Card.Body>
                            <Card.Title>{apartment.title}</Card.Title>
                            <Card.Text
                              style={{
                                width: "200px",
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                              }}
                            >
                              {apartment.description}
                            </Card.Text>
                            <Button variant="dark">
                              <Link to={`/my-apartments/${apartment.id}`}>
                                Ver más
                              </Link>
                            </Button>
                          </Card.Body>
                        </Card>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    content = (
      <div className="my-5 text-center">
        <h1>Actualmente no tienes viviendas</h1>
        <p className="regular-description">
          Puedes añadir una vivienda desde el menu de usuario o haciendo click
          aqui
        </p>
        <Button variant="dark">
          <Link to="/new-apartment">Añadir vivienda</Link>
        </Button>
      </div>
    );
  }

  return <>{content}</>;
}
