import React from "react";
import casa from "../images/casa.png";
import { Link, useParams } from "react-router-dom";
import { BsCalendar } from "react-icons/bs";
import { timestampToDate } from "../utils/auxiliars";
import { Button } from "react-bootstrap";

import BookingTable from "../components/BookingTable";
import PageNotFound from "./PageNotFound";

import "./ViewApartmentOwned.css";

export default function ViewApartmentOwned({
  userAccount,
  apartmentsList,
  bookingList,
  handleAction,
  handleApartmentState,
}) {
  const { apartmentId } = useParams();
  const apartment = apartmentsList[apartmentId];
  // Filter bookings by apartment. If there are no bookings availables, then return an empty array
  function filterBookingsByApartment() {
    try {
      return bookingList.filter(
        (booking) => booking.apartmentId === apartmentId
      );
    } catch (error) {
      return [];
    }
  }

  // Controlar si la ruta es valida o no
  let content;
  try {
    // In order to fix the issue with accounts change, we need to verify that the user is the owner of the property
    if (userAccount.toLowerCase() === apartment.owner.toLowerCase()) {
      content = (
        <div className="main-container">
          <div className="ApartmentPage">
            <div className="ApartmentBanner">
              <div className="Container -Container--full">
                <div className="ApartmentBanner-container">
                  <div className="ApartmentImage">
                    <div className="ApartmentImage-main">
                      <div className="ApartmentImage-main-container">
                        <img
                          className="Apartment-image"
                          src={casa}
                          alt="imagen demo"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="ApartmentPage-content">
              <div className="ApartmentProfile">
                <div className="Container Container--smGrow">
                  <div className="ApartmentSection">
                    <div className="ApartmentSection-content">
                      <div className="ApartmentHeader">
                        <div className="ApartmentHeader-main">
                          <div className="ApartmentHeader-title">
                            <h1 className="ApartmentHeader-title-text">
                              {apartment.title}
                            </h1>
                          </div>
                          <div className="ApartmentHeader-details">
                            <span className="ApartmentHeader-details-item">
                              #{apartment.id}
                            </span>
                          </div>
                          <div className="grid-container-edit">
                            <div className="grid-item-edit">
                              <Button variant="secondary">
                                <Link
                                  to={`/my-apartments/edition/${apartment.id}`}
                                >
                                  Editar
                                </Link>
                              </Button>
                            </div>
                            <div className="grid-item-edit">
                              {apartment.available ? (
                                <Button
                                  variant="secondary"
                                  onClick={function () {
                                    handleApartmentState(
                                      apartment.id,
                                      "disable-booking"
                                    );
                                  }}
                                >
                                  Inhabilitar
                                </Button>
                              ) : (
                                <Button
                                  variant="secondary"
                                  onClick={function () {
                                    handleApartmentState(
                                      apartment.id,
                                      "enable-booking"
                                    );
                                  }}
                                >
                                  Habilitar
                                </Button>
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="ApartmentHeader-owner">
                          <span class="ApartmentHeader-owner-details">
                            <span class="ApartmentHeader-owner-name">
                              {apartment.owner}
                            </span>
                            <span class="ApartmentHeader-owner-label">
                              Propietario
                            </span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="ApartmentSection">
                    <div className="ApartmentSection-header">
                      <h2 className="ApartmentSection-header-title">
                        Descripcion
                      </h2>
                    </div>
                    <div className="ApartmentSection-content">
                      <p>{apartment.description}</p>
                      <p>Ubicación: {apartment.direction}</p>
                      <div className="ApartmentInfo-uploadInfo">
                        <div className="ApartmentInfo-uploadInfo-list">
                          <BsCalendar className="ApartmentInfo-calendar-icon" />
                          <span>
                            <div className="ApartmentInfo-uploadInfo-title">
                              Publicada
                            </div>
                            <h3>
                              {apartment.creationDate
                                ? timestampToDate(apartment.creationDate)
                                : "No date to display"}
                            </h3>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="ApartmentSection">
                    <div className="ApartmentSection-header">
                      <h2 className="ApartmentSection-header-title">
                        Reservas
                      </h2>
                      <div className="ApartmentHeader-details">
                        <span className="ApartmentHeader-details-item">
                          Aqui puedes ver las reservas que tienes en tu
                          apartamento. Puedes reclamar los fondos una vez el
                          estado de la reserva este completado.
                          <br />
                          Los fondos disponibles representan el valor que
                          obtendrás si la reserva es confirmada y una vez se
                          complete la estancia.
                        </span>
                      </div>
                    </div>
                    <div className="ApartmentSection-content">
                      <BookingTable
                        bookingListFiltered={filterBookingsByApartment(
                          bookingList
                        )}
                        sendAction={(id, action) => {
                          handleAction(id, action);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      content = (
        <div className="my-5 text-center">
          <h1>Algo ha ido mal</h1>
          <p className="regular-description">Vuelve al inicio</p>
          <Button variant="dark">
            <Link to="/">Home</Link>
          </Button>
        </div>
      );
    }
  } catch (error) {
    content = <PageNotFound />;
  }

  return <>{content}</>;
}
