import React, { useState } from "react";

import "./NewApartment.css";
import { getUsdEthExchange, ethToWei } from "../utils/auxiliars";
import casa from "../images/casa.png";

export default function NewApartment({ onCreateApartment }) {
  const [data, setdata] = useState({
    title: "",
    description: "",
    direction: "",
    price: 0,
    codigo: 0,
  });

  const handleInputChange = (event) => {
    setdata({
      ...data,
      [event.target.name]: event.target.value,
    });
  };

  const enviardata = (event) => {
    event.preventDefault();
    console.log(
      "Creando apartmento...\nTitulo: " +
        data.title +
        "\nDescripcion: " +
        data.description +
        "\nDireccion " +
        data.direction +
        "\nPrecio/dia " +
        data.price +
        "\nCodigo: " +
        data.code
    );

    // Send value as wei
    let weiValue = ethToWei(data.price);
    onCreateApartment(
      data.title,
      data.description,
      data.direction,
      weiValue,
      data.code
    );
  };

  return (
    <>
      <div className="container">
        <div className="box">
          <div className="upperbox">
            <div className="circle"></div>
            <img src={casa} alt="icono" />
          </div>
          <div className="data">
            <h1 className="add-apartment">Añade tu vivienda</h1>
            <h3>Rellena los campos correspondientes</h3>
            <form className="column" onSubmit={enviardata}>
              <div className="field">
                <input
                  type="text"
                  placeholder="Titulo"
                  className="form-control"
                  onChange={handleInputChange}
                  name="title"
                  required
                ></input>
              </div>
              <div className="field">
                <textarea
                  type="text"
                  placeholder="Descripcion"
                  className="form-control"
                  onChange={handleInputChange}
                  name="description"
                  required
                ></textarea>
              </div>
              <div className="field">
                <input
                  type="text"
                  placeholder="Direccion"
                  className="form-control"
                  onChange={handleInputChange}
                  name="direction"
                  required
                ></input>
              </div>
              <div className="field">
                <input
                  type="text"
                  placeholder="Precio en ETH"
                  className="form-control"
                  onChange={handleInputChange}
                  name="price"
                  required
                ></input>
                <div>
                  <p className="usd-rate" id="usd-rate">
                    USD value: {data.price ? getUsdEthExchange(data.price) : 0}$
                  </p>
                </div>
              </div>
              <div className="field">
                <input
                  type="text"
                  minLength="6"
                  maxLength="6"
                  placeholder="Codigo de seguridad"
                  className="form-control"
                  onChange={handleInputChange}
                  name="code"
                  required
                ></input>
              </div>
              <button type="submit" className="btn-add">
                Añadir Vivienda
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
