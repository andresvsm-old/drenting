const Aplicacion = artifacts.require("./Aplicacion.sol");
const moment = require("moment");

const State = {
  Created: 0,
  Confirmed: 1,
  Withdrawable: 2,
  UserCanWithdraw: 3,
  OwnerCanWithdraw: 4,
  Rejected: 5,
  Completed: 6,
};

// generación de clave aleatoria de acceso
const generateRandom = () => {
  var result = "";
  var characters = "0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < 6; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

// Para usar la interfaz should
require("chai").use(require("chai-as-promised")).should();

contract("Aplicacion", ([deployer, owner, owner2, guest, guest2]) => {
  let app;

  before(async () => {
    app = await Aplicacion.deployed();
  });

  // Comprobar que esta el contrato desplegado
  describe("Deployment testing", async () => {
    it("Deployment successful", async () => {
      const address = await app.address;
      assert.notEqual(address, 0x0);
      assert.notEqual(address, "");
      assert.notEqual(address, null);
      assert.notEqual(address, undefined);
    });
  });

  describe("Apartment functionality test", async () => {
    let res, nAparments, accessCode;

    it("Create apartment", async () => {
      accessCode = generateRandom();
      res = await app.addApartment(
        "Apartamento Paris",
        "Buenas vistas y centrico",
        "Calle Sol",
        web3.utils.toWei("1", "Ether"),
        accessCode,
        { from: owner }
      );
      nAparments = await app.aCont();
      assert.equal(nAparments, 1, "Creation did update number of apartments");
    });

    it("Apartment info is correct", async () => {
      // Recuperamos el evento disparado al crear la vivienda
      const event = res.logs[0].args;

      // Hay que tener en cuenta que los valores que recibimos son del tipo BN
      assert.equal(event.id.toNumber(), 0, "ID is correct");
      assert.equal(event.title, "Apartamento Paris", "Name is correct");
      assert.equal(event.price, "1000000000000000000", "Price is correct");
      assert.equal(event.owner, owner, "Owner matches msg.sender");
      assert.equal(event.available, true, "Apartment is available");
      assert.equal(event.accessCode, accessCode, "accessCode is correct");
    });

    it("Multiple apartment creation", async () => {
      await app.addApartment(
        "Casa mata",
        "Excelente para 2 inquilinos",
        "Calle miragladiolos",
        web3.utils.toWei("0.5", "Ether"),
        generateRandom(),
        { from: owner }
      );

      await app.addApartment(
        "Piso planta 30",
        "Vistas increibles de la ciudad",
        "Calle principal",
        web3.utils.toWei("1.5", "Ether"),
        generateRandom(),
        { from: owner }
      );

      await app.addApartment(
        "Cabaña en las montañas",
        "Perfecto lugar para relajarse",
        "Avenida del parque",
        web3.utils.toWei("0.2", "Ether"),
        generateRandom(),
        { from: owner2 }
      );

      await app.addApartment(
        "Casa en la periferia",
        "Alejado del ruido de la ciudad",
        "Avenida 7",
        web3.utils.toWei("0.4", "Ether"),
        generateRandom(),
        { from: owner2 }
      );

      // Algunas pruebas para comprobar posibles fallos
      await await app.addApartment(
        "",
        "Buenas vistas y centrico",
        "Calle Sol",
        web3.utils.toWei("1", "Ether"),
        { from: owner }
      ).should.be.rejected;
      await await app.addApartment(
        "Apartamento Paris",
        "Buenas vistas y centrico",
        "Calle Sol",
        0,
        { from: owner }
      ).should.be.rejected;
    });

    it("Number of apartments should be 5", async () => {
      nAparments = await app.aCont();
      assert.equal(nAparments, 5, "Creation did update number of apartments");
    });

    it("Owner should be able to pause bookings on his apartment", async () => {
      await app.pauseBooking(1, { from: owner });
      let apartmentPaused = await app.apartments(1);
      assert.equal(apartmentPaused.available, false, "Availability did change");
    });

    it("Owner should be able to resume bookings on his apartment", async () => {
      await app.resumeBooking(1, { from: owner });
      let apartmentResumed = await app.apartments(1);
      assert.equal(apartmentResumed.available, true, "Availability did change");
    });

    it("Owner should be able to modify his apartment", async () => {
      let newAccessCode = generateRandom();
      await app.changeApartmentInfo(
        4,
        "Casa en la periferia modificada",
        "Alejado del ruido urbano",
        "Avenida 9",
        web3.utils.toWei("0.3", "Ether"),
        newAccessCode,
        { from: owner2 }
      );
      let apartmentModified = await app.apartments(4);
      assert.equal(apartmentModified.id.toNumber(), 4, "ID is correct");
      assert.equal(
        apartmentModified.title,
        "Casa en la periferia modificada",
        "Name is correct"
      );
      assert.equal(
        apartmentModified.price,
        web3.utils.toWei("0.3", "Ether"),
        "Price is correct"
      );
      assert.equal(apartmentModified.owner, owner2, "Owner matches msg.sender");
      assert.equal(apartmentModified.available, true, "Apartment is available");
      assert.equal(
        apartmentModified.accessCode,
        newAccessCode,
        "accessCode is correct"
      );
    });
  });

  describe("Booking functionality test", async () => {
    let apartment, bookingLog, nBookings;
    let deposit = 6000000000000000;
    let startDateUnix = 1646935200; // March 10th 2022 Entry
    let endDateUnix = 1647090000; // March 12th 2022 Exit
    let mar8 = 1646762400; // Entry
    let mar10 = 1646917200; // Exit
    let mar12 = 1647108000; // Entry
    let mar14 = 1647262800; // Exit
    let days = Math.floor((endDateUnix - startDateUnix) / (60 * 60 * 24)) + 1;

    // Obtener la primera vivienda
    before(async () => {
      apartment = await app.apartments(0);
    });

    it("Create booking for a day", async () => {
      let bookingPrice = parseInt(apartment.price) * days + deposit;

      bookingLog = await app.book(0, startDateUnix, endDateUnix, {
        from: guest,
        value: bookingPrice,
      });
      nBookings = await app.bCont();

      const event2 = bookingLog.logs[0].args;

      assert.equal(event2.id.toNumber(), 0, "Booking ID is correct");
      assert.equal(event2.state, State.Created, "State is correct");
      assert.equal(
        event2.startDate,
        `${startDateUnix}`,
        "Start date is correct"
      );
      assert.equal(event2.endDate, `${endDateUnix}`, "End date is correct");
      assert.equal(event2.nDays, days, "Number of days is correct");
      assert.equal(event2.total, bookingPrice, "Pricing is correct");
    });

    it("Contract should store the funds of a booking", async () => {
      let balance = await web3.eth.getBalance(app.address);
      let booking = await app.bookings(0);
      assert.equal(balance, booking.total, "Balance is correct");
    });

    it("Multiple booking creation", async () => {
      let bookingPrice = parseInt(apartment.price) * days + deposit;

      // Should be able to book from March 8th-10th and from March 12th to 14th
      // The difference is that mar10 is at 12:00 and mar12 is at 17:00, therefore startDateUnix (17:00) and endDateUnix (12:00) won't fail
      await app.book(0, mar8, mar10, { from: guest, value: bookingPrice });
      await app.book(0, mar12, mar14, { from: guest, value: bookingPrice });

      // Another guest should be able to book the same dates if the previous booking is not confirmed
      // This booking will have id 3
      await app.book(0, startDateUnix, endDateUnix, {
        from: guest2,
        value: bookingPrice,
      });
    });

    it("Booking confirmation", async () => {
      let bookingPrice = parseInt(apartment.price) * days + deposit;
      await app.confirmBooking(0, { from: owner });
      // Once confirmed, the dates can't be booked anymore and the bookings which dates
      // are within the confirmed booking dates must be rejected

      let rejectedBooking = await app.bookings(3);
      assert.equal(
        rejectedBooking.state,
        State.Rejected,
        "Booking with the same dates has been rejected"
      );

      // Cant make the same booking because of the day (March 10th -12th)
      await await app.book(0, startDateUnix, endDateUnix, {
        from: guest2,
        value: bookingPrice,
      }).should.be.rejected;

      // Cant create a booking if days in between are booked (March 8th - 14th)
      await await app.book(0, mar8, mar14, { from: guest, value: bookingPrice })
        .should.be.rejected;

      let booking = await app.bookings(0);
      let userFunds = await app.getUserPendingWithdraws(guest, 0);
      let ownerFunds = await app.getOwnerPendingWithdraws(owner, 0);
      assert.equal(booking.state, State.Confirmed, "State is correct");
      assert.equal(userFunds, deposit, "User funds are correct");
      assert.equal(
        ownerFunds,
        booking.total - deposit,
        "Owner funds are correct"
      );
    });

    it("Booking rejection", async () => {
      await app.rejectBooking(1, { from: owner });
      let userFunds = await app.getUserPendingWithdraws(guest, 1);
      let ownerFunds = await app.getOwnerPendingWithdraws(owner, 1);
      let booking = await app.bookings(1);
      assert.equal(booking.startDate, "0", "La fecha se pone a 0");
      assert.equal(booking.state, State.Rejected, "State is correct");
      // In this case we use template string to check the correct funds, it doesn't support BN bigger than 53 bits
      assert.equal(
        `${userFunds}`,
        `${booking.total}`,
        "User funds are correct"
      );
      assert.equal(ownerFunds, 0, "Owner funds are correct");
    });

    it("User should be able to withdraw a rejected booking", async () => {
      let initial_balance = await web3.eth.getBalance(guest); // string
      let tx = await app.withdrawFunds(1, { from: guest });
      let gasUsed = tx.receipt.gasUsed;
      let gasPrice = (await web3.eth.getTransaction(tx.tx)).gasPrice;
      let new_balance = await web3.eth.getBalance(guest);
      let booking = await app.bookings(1);

      // Si restamos al balance final el precio de la reserva, deberiamos tener el balance inicial menos las fees
      assert.equal(
        new_balance - booking.total,
        initial_balance - `${gasPrice * gasUsed}`,
        "Se ha recuperado el valor de la reserva"
      );
      assert.equal(booking.state, State.Completed, "State is correct");
    });

    it("User cancels a booking confirmed and now > startDate - 10 && now < startDate", async () => {
      let apartment = await app.apartments(1);

      let nextday = new Date();
      let endDay = new Date();

      nextday.setDate(nextday.getDate() + 1);
      endDay.setDate(endDay.getDate() + 3);
      nextday.setHours(17, 0, 0);
      endDay.setHours(12, 0, 0);

      let nextDayUnix = moment(nextday).unix();
      let endDayUnix = moment(endDay).unix();

      let days = Math.floor((endDayUnix - nextDayUnix) / (60 * 60 * 24)) + 1;

      let bookingPrice = parseInt(apartment.price) * days + deposit;

      let bookingEvent = await app.book(1, nextDayUnix, endDayUnix, {
        from: guest,
        value: bookingPrice,
      });

      let bookingLog = bookingEvent.logs[0].args;
      await app.confirmBooking(bookingLog.id.toNumber(), { from: owner });
      await app.cancelBooking(bookingLog.id.toNumber(), { from: guest });

      let userFunds = await app.getUserPendingWithdraws(
        guest,
        bookingLog.id.toNumber()
      );

      let booking = await app.bookings(bookingLog.id.toNumber());
      assert.equal(booking.state, State.Withdrawable, "State is correct");
      assert.equal(userFunds, bookingPrice - deposit, "User funds are correct");
    });

    it("User cancels a booking confirmed and now < startDate - 10", async () => {
      let apartment = await app.apartments(1);

      let nextday = new Date();
      let endDay = new Date();

      nextday.setDate(nextday.getDate() + 15);
      endDay.setDate(endDay.getDate() + 17);
      nextday.setHours(17, 0, 0);
      endDay.setHours(12, 0, 0);

      let nextDayUnix = moment(nextday).unix();
      let endDayUnix = moment(endDay).unix();

      let days = Math.floor((endDayUnix - nextDayUnix) / (60 * 60 * 24)) + 1;

      let bookingPrice = parseInt(apartment.price) * days + deposit;

      let bookingEvent = await app.book(1, nextDayUnix, endDayUnix, {
        from: guest,
        value: bookingPrice,
      });

      let bookingLog = bookingEvent.logs[0].args;

      await app.confirmBooking(bookingLog.id.toNumber(), { from: owner });
      await app.cancelBooking(bookingLog.id.toNumber(), { from: guest });

      let userFunds = await app.getUserPendingWithdraws(
        guest,
        bookingLog.id.toNumber()
      );

      let booking = await app.bookings(bookingLog.id.toNumber());
      assert.equal(booking.state, State.UserCanWithdraw, "State is correct");
      assert.equal(userFunds, bookingPrice, "User funds are correct");
    });

    it("User cancels a booking that hasn't been confirmed", async () => {
      let apartment = await app.apartments(1);

      let nextday = new Date();
      let endDay = new Date();

      nextday.setDate(nextday.getDate() + 15);
      endDay.setDate(endDay.getDate() + 17);
      nextday.setHours(17, 0, 0);
      endDay.setHours(12, 0, 0);

      let nextDayUnix = moment(nextday).unix();
      let endDayUnix = moment(endDay).unix();

      let days = Math.floor((endDayUnix - nextDayUnix) / (60 * 60 * 24)) + 1;

      let bookingPrice = parseInt(apartment.price) * days + deposit;

      let bookingEvent = await app.book(1, nextDayUnix, endDayUnix, {
        from: guest,
        value: bookingPrice,
      });

      let bookingLog = bookingEvent.logs[0].args;

      await app.cancelBooking(bookingLog.id.toNumber(), { from: guest });

      let userFunds = await app.getUserPendingWithdraws(
        guest,
        bookingLog.id.toNumber()
      );

      let booking = await app.bookings(bookingLog.id.toNumber());
      assert.equal(booking.state, State.UserCanWithdraw, "State is correct");
      assert.equal(userFunds, bookingPrice, "User funds are correct");
    });
    /**
     * As for check in and check out functions, they can only be tested on live
     *
     * The next tests should run removing the line: require(now >= bookings[bID].startDate);
     * from checkin function.
     */
    it("User should be able to check in and view accessCode", async () => {
      await app.checkin(0, { from: guest });
      let booking = await app.bookings(0);
      let apartment = await app.apartments(booking.apartmentId);
      let code = await app.viewCode(booking.id, guest);

      assert.equal(booking.state, State.Withdrawable, "State is correct");
      assert.equal(apartment.guest, guest, "Apartment guest is set");
      assert.equal(code, apartment.accessCode, "Access code are correct");
    });

    it("User checks out before owner claims his funds", async () => {
      await app.checkout(0, { from: guest });
      let booking = await app.bookings(0);
      let userFunds = await app.getUserPendingWithdraws(guest, booking.id);
      let apartment = await app.apartments(booking.apartmentId);

      assert.equal(booking.state, State.OwnerCanWithdraw, "State is correct");
      assert.equal(userFunds, 0, "User funds are correct");
      assert.equal(
        apartment.guest,
        "0x0000000000000000000000000000000000000000",
        "User funds are correct"
      );
    });

    it("Owner claims his funds", async () => {
      await app.withdrawBooking(0, { from: owner });
      let booking = await app.bookings(0);
      let ownerFunds = await app.getOwnerPendingWithdraws(guest, booking.id);
      assert.equal(booking.state, State.Completed, "State is correct");
      assert.equal(ownerFunds, 0, "User funds are correct");
    });

    it("User checks out after owner claims his funds", async () => {
      // Using the booking with id 2
      await app.confirmBooking(2, { from: owner });
      await app.checkin(2, { from: guest });
      await app.withdrawBooking(2, { from: owner });
      await app.checkout(2, { from: guest });
      let booking = await app.bookings(2);
      let userFunds = await app.getUserPendingWithdraws(guest, booking.id);
      let ownerFunds = await app.getOwnerPendingWithdraws(owner, booking.id);
      assert.equal(booking.state, State.Completed, "State is correct");
      assert.equal(userFunds, 0, "User funds are correct");
      assert.equal(ownerFunds, 0, "User funds are correct");
    });
  });
});
